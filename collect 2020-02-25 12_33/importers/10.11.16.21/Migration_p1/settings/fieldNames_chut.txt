﻿{
  "cf_isMigrated": { "type": "function", "value": "'true'" },
  "cf_containerType": { "type": "function", "value": "'коллекция'" },
  
  "status": "CASE WHEN CHUTIER.INTERNET = 1 THEN 'выпущено на сайт' ELSE NULL END",
  
  "cf_headlineRu": "CHUTIER.NOM",
  "cf_headlineEn": "CHUTIER.TEXT_RETROSPECTIVE",
  "cf_wireSections": "CHUTIER.SECTION",
  "cf_keyAssetId": "CHUTIER.ID_OBJET_DEFAULT",
  "cf_itemCount": "CHUTIER.NBIMAGES",
  "assetCreated": "TO_CHAR(CHUTIER.DATE_CREAT,'YYYY-MM-DD HH24:MI:SS')",
  "iptcCreated": "TO_CHAR(CHUTIER.DATE_CREAT,'YYYY-MM-DD HH24:MI:SS')",
  "assetCreator": "CHUTIER.ID_USER",
  "public": "case when CHUTIER.EST_PUBLIC = 1 then 'true' else NULL end",
  "hidden": "case when CHUTIER.INTERNET <> 1 then 'true' else NULL end",
  "сf_urgencyRef": "CHUTIER.STATUT_ALBUM",
  "cf_objectId": "CHUTIER.ID_CHUTIER",

  "ax_ID_CHUTIER": "CHUTIER.ID_CHUTIER",
  "ax_itemCount": "CHUTIER.NBIMAGES"
}