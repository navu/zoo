﻿left join {0}{1}{0}.STOCKS on LIASSES.ID_STOCK_LIASSE=STOCKS.ID_STOCK
left join (
    select * from {0}{1}{0}.liasses_i18n
    where id_liasse_i18n in (
        select max(id_liasse_i18n) from {0}{1}{0}.liasses_i18n group by id_liasse
    )
) liasses_i18n_fix on liasses.id_liasse = liasses_i18n_fix.id_liasse