#charset=UTF-8
#
# Number of Elvis nodes that are supposed to form this cluster.
#
# It is important to use the correct value here.
#
cluster.nodeCount=12

#
# Number of Elvis nodes in the cluster that are configured with searchDataEnabled=true.
#
# It is important to use the correct value here.
#
# This setting is used to prevent split-brain situations and is 'converted' to an Elasticsearch config setting
# 'discovery.zen.minimum_master_nodes'. When nodes start and join together, they won't form a working cluster until at
# least half this amount plus one are joined (cluster.searchNodeCount / 2 + 1). This ensures the Elasticsearch cluster\
# will only be formed by a majority of the nodes. For details see: http://www.elasticsearch.org/guide/reference/modules/discovery/zen/
#
# This setting also influences Elasticsearch index configuration:
#
# - index.numberOfShards: the number of shards for an index by default matches the searchNodeCount.
#   One shard per search node is best from performance point of view. If you expect your number of search nodes to grow,
#   then configure 'index.numberOfShards' to match your expected maximum number of search nodes. If your indices have
#   already been created and you change the number of search nodes, or change this setting, then you will need to
#   re-index by incrementing the index revision to apply the setting.
#   For details see the comment for the 'index.numberOfShards' property by listing the config properties from the API.
#
# - defaultNumberOfReplicas: when searchNodeCount = 1, the default number of replicas will be 0, searchNodeCount > 1,
#   default number of replicas will be 1.
#
# Important:
#
# - When you increase the number of search nodes, make sure the existing nodes are started at least 15 seconds before
#   you start any new search nodes. Otherwise existing index data may not be loaded correctly.
#
# - Do not reduce the number of search nodes once you have been running with more that 1 node.
#   If you plan to do so on a production system, please contact support@woodwing.com.
#
cluster.searchNodeCount=7

#
# Email address that will receive server warnings.
#
serverAdminEmail=elvis_notifications@tass.ru

#
# To improve reliability of warning emails, you may have to configure your SMTP server.
# Otherwise mails might get marked as SPAM. Setting the host is usually enough. If needed, credentials can be specified.
#
smtpHost=smtp.tass.ru

#
# To use SMTPS over SSL, configure: smtpProtocol=smtps
#
smtpProtocol=

#
# Port to use for connecting to smtpHost. Use port 465 or 587 for secure SMTPS.
#
smtpPort=25

#
# Username used to connect to smtpHost.
#
smtpUsername=

#
# Password used to connect to smtpHost.
#
smtpPassword=

locale=ru_RU
analyzerLocale=ru_RU
exiftoolCommandLineArguments = -a -G1 -u -m -n -j -charset IPTC=cp1251 -use mwg
analytics.enabled = false
defaultClientLocale = ru_RU
fileSystemReliability.makeLocalCopy = true
fileSystemReliability.retryOnError = true
loginHelpUrl = https://helpcenter.woodwing.com/hc/en-us/articles/360000758563-Elvis-6-Quick-Start-guide
maxRequestTries = 3
security.runningBehindLoadBalancer = true
webSocketEnabled = true
httpsEnabled = true
httpsKeyManagerPassword = OBF:1kqr1s4i1bv61ob71g131ktv1kqz1g2f1odn1bxo1s2k1ku3
httpsKeyStorePassword = OBF:1kqr1s4i1bv61ob71g131ktv1kqz1g2f1odn1bxo1s2k1ku3
httpsKeyStore = ${configDir}/elvis-ssl.jks
useOnlyRemoteProcessingWhenAvailable = true
security.login.userTypeRequired = true
samlIdpMetadataUrl = https://sso.tass.ru/FederationMetadata/2007-06/FederationMetadata.xml
samlClientDefaultLogin = true
samlSpEntityId = https://dam.tass.ru
index.numberOfShards = 7
configAdminPage.directoryExcludePattern = **{/clean-example-config*,/plugins/plugin_base,/plugins/active/internal,/plugins/samples}
configAdminPage.extensionIncludePattern = **.{csv,txt,properties,xsl,xml,json,js,html}
autoOrganizeZoneRoot = false
enabledUsageLogActions =*
session.timeout = 2880
proClientSortFields = name,created,fileSize,assetCreated,assetModified,relevance,downloadCount,viewCount,cf_objectId
