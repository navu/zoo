﻿
left outer join {0}{1}{0}.DESCRIPTION ON     OBJETS.ID_OBJET = DESCRIPTION.ID_OBJET 
LEFT OUTER JOIN {0}{1}{0}.FICHIERS "FICHIERS_HI" ON OBJETS.ID_OBJET = FICHIERS_HI.ID_OBJET and FICHIERS_HI.ID_TYPE = 4 
LEFT OUTER JOIN {0}{1}{0}.FICHIERS "FICHIERS_LO" ON OBJETS.ID_OBJET = FICHIERS_LO.ID_OBJET and FICHIERS_LO.ID_TYPE = 1 
left outer join {0}{1}{0}.IPTC ON            OBJETS.ID_OBJET = IPTC.ID_OBJET 
left outer join {0}{1}{0}.LONGDESC ON        OBJETS.ID_OBJET = LONGDESC.ID_OBJET 
left outer join {0}{1}{0}.OBJETS_I18N ON     OBJETS.ID_OBJET = OBJETS_I18N.ID_OBJET 
left outer join {0}{1}{0}.STOCKS ON OBJETS.ID_STOCK = STOCKS.ID_STOCK 
LEFT OUTER JOIN {0}{1}{0}.VOLUMES "VOLUMES_HI" ON VOLUMES_HI.ID_VOLUME = FICHIERS_HI.ID_VOLUME 
LEFT OUTER JOIN {0}{1}{0}.VOLUMES "VOLUMES_LO" ON VOLUMES_LO.ID_VOLUME = FICHIERS_LO.ID_VOLUME 
LEFT OUTER JOIN {0}{1}{0}.COUNTRIES_I18N "COUNTRY_RU" ON IPTC.COUNTRY_CODE=COUNTRY_RU.COUNTRY_CODE AND COUNTRY_RU.LANGUAGE=18 
LEFT OUTER JOIN {0}{1}{0}.COUNTRIES_I18N "COUNTRY_EN" ON IPTC.COUNTRY_CODE=COUNTRY_EN.COUNTRY_CODE AND COUNTRY_EN.LANGUAGE=2 
left join 
            ( select
                id_objet,
                '"'||
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  rtrim(xmlagg(xmlelement(e,CASE WHEN ID_LANGUAGE=18 THEN IPTC_KEYWORD END,'$--$').extract('//text()') order by iptc_keyword).GetClobVal(),'$--$')
  ,'\','\\')
  ,chr(9),'\t' )
  ,chr(10),'\n' )
  ,chr(13),'\r' )
  ,chr(38)||'quot;','\"' )
  ,chr(38)||'apos;','''' )
  ,chr(38)||'amp;','&' )
  ,chr(38)||'lt;','<' )
  ,chr(38)||'gt;','>' )
  ,'$--$','","' )||'"' as tags_ru,
                 '"'||
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  rtrim(xmlagg(xmlelement(e,CASE WHEN ID_LANGUAGE=2 THEN IPTC_KEYWORD END,'$--$').extract('//text()') order by iptc_keyword).GetClobVal(),'$--$')
  ,'\','\\')
  ,chr(9),'\t' )
  ,chr(10),'\n' )
  ,chr(13),'\r' )
  ,chr(38)||'quot;','\"' )
  ,chr(38)||'apos;','''' )
  ,chr(38)||'amp;','&' )
  ,chr(38)||'lt;','<' )
  ,chr(38)||'gt;','>' )
  ,'$--$','","' )||'"' as tags_en
              from 
                {0}{1}{0}.iptc_keywords 
              group by 
                id_objet 
            ) iptc_keywords_R on objets.id_objet=iptc_keywords_R.id_objet 
left join 
            ( select 
                id_objet,
                 '"'||
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  LISTAGG(CASE WHEN ID_LANGUAGE=18 THEN IPTC_CATEGORIE END, '$--$') WITHIN GROUP (ORDER BY IPTC_CATEGORIE)
  ,'\','\\')
  ,chr(9),'\t' )
  ,chr(10),'\n' )
  ,chr(13),'\r' )
  ,'"','\"')
  ,'$--$','","')
  ||'"' as categorie_ru,
                '"'||
  replace(
  replace(
  replace(
  replace(
  replace(
  replace(
  LISTAGG(CASE WHEN ID_LANGUAGE=2 THEN IPTC_CATEGORIE END, '$--$') WITHIN GROUP (ORDER BY IPTC_CATEGORIE)
  ,'\','\\')
  ,chr(9),'\t' )
  ,chr(10),'\n' )
  ,chr(13),'\r' )
  ,'"','\"')
  ,'$--$','","')
  ||'"' as categorie_en
              from 
                {0}{1}{0}.iptc_categories 
              group by 
                id_objet 
            ) iptc_categories_R on objets.id_objet=iptc_categories_R.id_objet 
left join 
			( select
				id_objet,
				LISTAGG(id_chutier, ',') WITHIN GROUP (ORDER BY id_chutier) as id_pos 
			  from 
				{0}{1}{0}.contains_objects
			  group by 
				id_objet 
			) obj_relations on objets.id_objet=obj_relations.id_objet 
 

                            