## 1.0.7408.39280 (2020/04/13)

Изменена конвертация полей: cf_releaseDate, cf_releaseTime, cf_releaseDateTime

```
bool IsDateMatchesTemplate = false;
         
            if (result.ContainsKey(elvMetaData.cf_releaseDate))
            {
                if (Regex.IsMatch(result[elvMetaData.cf_releaseDate], DATE_PATTERN._4_2_2))
                {
                    result[elvMetaData.cf_releaseDate] = result[elvMetaData.cf_releaseDate].Replace('.', '-');
                    IsDateMatchesTemplate = true;
                }
                else
                    if (Regex.IsMatch(result[elvMetaData.cf_releaseDate], DATE_PATTERN._8))
                    {
                        result[elvMetaData.cf_releaseDate] = result[elvMetaData.cf_releaseDate].Insert(6, "-").Insert(4, "-");
                        IsDateMatchesTemplate = true;
                    }
                    else
                        if (Regex.IsMatch(result[elvMetaData.cf_releaseDate], DATE_PATTERN._YYYY_MM))
                        {
                            result[elvMetaData.cf_releaseDate] = result[elvMetaData.cf_releaseDate].Insert(8, "01");
                            IsDateMatchesTemplate = true;
                        }
                        else
                            Append(ref result, elvMetaData.cf_migrationIssues, DATE_PATTERN.NON_MATCH_RELDATE);
            }
           
           
            if (result.ContainsKey(elvMetaData.cf_releaseTime))
            {
                if (Regex.IsMatch(result[elvMetaData.cf_releaseTime], TIME_PATTERN._) || Regex.IsMatch(result[elvMetaData.cf_releaseTime], TIME_PATTERN._1))
                {
                    result[elvMetaData.cf_releaseTime] = result[elvMetaData.cf_releaseTime].Insert(4, ":").Insert(2, ":");
                }
                else
                    if (Regex.IsMatch(result[elvMetaData.cf_releaseTime], TIME_PATTERN._2))
                    {
                        result[elvMetaData.cf_releaseTime] = result[elvMetaData.cf_releaseTime].Remove(10, 1).Insert(7, "0").Insert(4, ":").Insert(2, ":");
                    }
                    else
                        if (Regex.IsMatch(result[elvMetaData.cf_releaseTime], TIME_PATTERN._3))
                        {
                            result[elvMetaData.cf_releaseTime] = result[elvMetaData.cf_releaseTime].Insert(8, "+0");
                        }
                        else
                            if (!Regex.IsMatch(result[elvMetaData.cf_releaseTime], TIME_PATTERN._4))
                            {
                                result[elvMetaData.cf_releaseTime] = TIME_PATTERN._00_00_00;
                                Append(ref result, elvMetaData.cf_migrationIssues, TIME_PATTERN.NON_MATCH_RELTIME);
                            }
            }
            else
            {
                result.Add(elvMetaData.cf_releaseTime, TIME_PATTERN._00_00_00);
                Append(ref result, elvMetaData.cf_migrationIssues, TIME_PATTERN.NON_MATCH_RELTIME);
            }
           
            if (IsDateMatchesTemplate && result.ContainsKey(elvMetaData.cf_releaseDate) && result.ContainsKey(elvMetaData.cf_releaseTime))
                result.Add(elvMetaData.cf_releaseDateTime, string.Format("{0}T{1}", result[elvMetaData.cf_releaseDate], result[elvMetaData.cf_releaseTime]));
            else
                if (IsDateMatchesTemplate && result.ContainsKey(elvMetaData.cf_releaseDate))
                    result.Add(elvMetaData.cf_releaseDateTime, string.Format("{0}T00:00:00", result[elvMetaData.cf_releaseDate]));
```