# git
#
# create local git
#    git init .
#
# add git
#    git remote add origin https://navu@bitbucket.org/navu/zoo.git
#
# commit changes
#    git add . && git commit -m "initial commit3" && git push origin master

# run ./collect\ data.sh comment to change git message

# git add . && git commit -m "Photos part 1" && git push origin master
# git add . && git commit -m "Videos 2019" && git push origin master

die() {
    echo Usage: $( basename "$0" ) options
    echo Options are:
	echo "    -f folder  Name of collect folder"
	echo "    -c commit  Commit message"
    exit 0
}

[[ $# -eq 0 ]] && die forever

while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		-f)
		c_path="$2"
		shift # past argument
		shift # past value
		;;
		-c)
		c_message="$2"
		shift # past argument
		shift # past value
		;;
		*)    # unknown option
		shift # past argument
		;;
	esac
done

[[ -z "$c_path" ]] && die forever
[[ -z "$c_message" ]] && die forever

echo Using:
echo "    Collect folder:  $requests"
echo "    Commit message:  $concurrency"
echo

cd "$(dirname "$0")"
mkdir -p "${c_path}"

function logme() {
	echo "$*"
	echo "$*" >> "${c_path}/readme.md"
}

function collect_node() {
	elvis_ip=$( jq -r ".ip" <<< "$*")
	elvis_name=$( jq -r ".name" <<< "$*")
	
	
	mkdir -p "${c_path}/nodes/${elvis_ip}" 

	scp -q root@"${elvis_ip}":/srv/elvis-server/config/*config.properties.txt "${c_path}/nodes/${elvis_ip}/"
	
	search_string="searchDataEnabled"	
	search_result=$( cat "${c_path}/nodes/${elvis_ip}/node-config.properties.txt" | grep "^$search_string=true" -o )
	[ -n "$search_result" ] && searchDataEnabled="Search/Web API" || searchDataEnabled=

	search_string="processingEnabled"	
	search_result=$( cat "${c_path}/nodes/${elvis_ip}/node-config.properties.txt" | grep "^$search_string=true" -o )
	[ -n "$search_result" ] && processingEnabled=Processing || processingEnabled=

	search_string="jobRunnerEnabled"	
	search_result=$( cat "${c_path}/nodes/${elvis_ip}/node-config.properties.txt" | grep "^$search_string=true" -o )
	[ -n "$search_result" ] && jobRunnerEnabled="Job Runner" || jobRunnerEnabled=
	
	elvis_roles=($searchDataEnabled $processingEnabled $jobRunnerEnabled)
	elvis_roles=$( IFS=$'/'; echo "${elvis_roles[*]}" )
	
	centos_version=$( ssh root@"${elvis_ip}" "cat /etc/centos-release")
	
	centos_cpu=$( ssh root@"${elvis_ip}" "lscpu | grep '^CPU(s)' | sed 's/.*:\ *//g'")

	centos_mem=$( ssh root@"${elvis_ip}" "lsmem | grep Total\ online | sed 's/.*:\ *//g'")
	
	logme "|"
	logme "**Имя узла**|**$elvis_name**"
	logme "Роли|$elvis_roles"

	logme "Операционная система|$centos_version"
	logme "IP адрес|$elvis_ip"
	logme "Количество ядер|$centos_cpu"
	logme "Объем памяти|$centos_mem"

}

auth=$( printf admin:changemenow | base64 )

elvis_metrics=$( curl -s https://dam.tass.ru/services/system/metrics?authcred=YWRtaW46Y2hhbmdlbWVub3c= | jq . )
elvis_ping=$( curl -s "https://dam.tass.ru/services/ping" | jq . )


elvis_version=$( printf "$elvis_ping" | jq -r ".version" )
elvis_name=$( jq -r '.cluster.name' <<< "$elvis_metrics" )
elvis_nodeCount=$( jq '.nodes | length' <<< "$elvis_metrics" )


logme "# Протокол импорта"
logme 
logme "|"
logme " --- | ---"
logme "Имя кластера|$elvis_name"
logme "Версия Elvis|$elvis_version"
logme "Количество узлов|$elvis_nodeCount"

#collecting nodes configs
for node in $( jq '.nodes | keys[]' <<< "$elvis_metrics"); do
	datanode=$( jq ".nodes[$node]" <<< "$elvis_metrics")
	collect_node "$datanode"
done

logme "|"
logme "**Документы Google:**"

# get DAM: разделы (библиотеки) ТФК
tmp_id="13yA47yyJgoc22VHIUK4p8u2EeuoU9YJrsuqNhK29_bQ"
tmp_name="DAM: разделы (библиотеки) ТФК"
logme "$tmp_name"
curl -s "https://docs.google.com/spreadsheets/d/$tmp_id/export?format=xlsx&id=$tmp_id" -o "${c_path}/$tmp_name.xlsx"
curl -s "https://docs.google.com/spreadsheets/d/$tmp_id/export?format=pdf&id=$tmp_id" -o "${c_path}/$tmp_name.pdf"

# get DAM: Реестр метаданных
tmp_id="1X1Q4XtpGVUjSl88kKSGJ2v6OgZqJjzCpnmLIHettrUE"
tmp_name="DAM: Реестр метаданных"
logme "$tmp_name"
curl -s "https://docs.google.com/spreadsheets/d/$tmp_id/export?format=xlsx&id=$tmp_id" -o "${c_path}/$tmp_name.xlsx"
curl -s "https://docs.google.com/spreadsheets/d/$tmp_id/export?format=pdf&id=$tmp_id" -o "${c_path}/$tmp_name.pdf"


#collecting executables
logme "|"
logme "**Импортеры:**"

for elvis_node in "10.11.16.21" "10.11.16.22"; do
	logme "Адрес узла|${elvis_node}"
	mkdir -p "${c_path}/importers/${elvis_node}" 
	scp -r abit@${elvis_node}:Migration_* "${c_path}/importers/${elvis_node}/"
done

git add . 
git commit -m "$c_message"
git push origin master

exit 

# old-style storage

echo
echo Compressing
cd "$c_path"
zip -q -r "../$c_path" . -x ".DS_Store"

echo Cleaning
cd ..
rm -r "$c_path"
