#charset=UTF-8
#
# LDAP Configuration
#
# Note: Knowledge about LDAP in general and your LDAP environment in specific 
# is required to configure LDAP in Elvis. It can be quite a challenge to find
# the correct search settings depending on how Active Directory or LDAP is setup.
#
# LDAP Authentication is disabled by default, to enable uncomment one of the
# templates below by remove the hashes '#', and modify the settings to reflect
# your environment.
# If you need to use a backslash in any of the properties make sure to escape
# it with a second backslash. So \ should be entered as: \\
#
# A good tool to browse an LDAP server is Apache directory studio:
# http://directory.apache.org/studio/
#
# Possible configuration settings:
#
# - ldapAuthenticationEnabled=false
# - ldapServerUrl=
# - ldapDirectoryType=ActiveDirectory | OpenDirectory
# - ldapAuthenticationMechanism=ANONYMOUS				(ANONYMOUS,SIMPLE,DIGEST-MD5) The authentication mechanism to use when binding to the LDAP server.
# - ldapManagerDn=
# - ldapManagerPassword=
# - ldapGroupSearchBase=ou=groups
# - ldapGroupSearchFilter=(member={0})
# - ldapGroupRoleAttribute=cn
# - ldapGroupObjectClassRestrictions=group				(Optional filter for objectClass, translated into 'or' filter)
# - ldapUserSearchBase=ou=people
# - ldapUserSearchFilter=(uid={0})
# - ldapUserObjectClassRestrictions=user,person			(Optional filter for objectClass, translated into 'or' filter)
#



#
# Example for MS Active Directory server
#
# Note: the exact searches and filters to use greatly depend
# on how the active directory is set up. Be sure to check out
# the variations below or to use the Apache directory studio
# to get a look into the LDAP structure and test your searches.
#

#ldapAuthenticationEnabled=true
#ldapServerUrl=ldap://yourip:3268/dc=yourdomain,dc=lan
#ldapDirectoryType=ActiveDirectory
#ldapAuthenticationMechanism=SIMPLE
#ldapManagerDn=cn=Administrator,cn=Users,dc=yourdomain,dc=lan
#ldapManagerPassword=secret
#ldapGroupSearchBase=
#ldapGroupSearchFilter=(&(objectClass=group)(member={0}))
#ldapGroupRoleAttribute=sAMAccountName
#ldapUserSearchBase=cn=Users
#ldapUserSearchFilter=(&(objectClass=person)(sAMAccountName={0}))
#ldapUserObjectClassRestrictions=person

#
# Example variations for MS Active Directory server
#
# Try these settings or variations of them if the standard settings
# do not work. Or use Apache directory studio to find out which values
# you need to enter.
#

#ldapGroupSearchBase=cn=Group
#ldapGroupSearchFilter=(member={0})
#ldapGroupRoleAttribute=sAMAccountName
#ldapUserSearchBase=ou=accounts
#ldapUserSearchFilter=(&(objectClass=person)(sAMAccountName={0}))
#ldapUserObjectClassRestrictions=person

#ldapGroupSearchBase=ou=N&S
#ldapGroupSearchFilter=(&(objectClass=group)(member={0}))
#ldapGroupRoleAttribute=sAMAccountName
#ldapUserSearchBase=ou=N&S
#ldapUserSearchFilter=(&(objectClass=person)(sAMAccountName={0}))

#
# Authenticate using email address instead of account name:
#

#ldapUserSearchFilter=(&(objectClass=person)(mail={0}))


#
# Authenticate and use as internal username UserPrincipalName
# instead of sAMAccountName
# ldapUsernameProperty is now supported for AD only.
#

#ldapUserSearchFilter=(&(objectClass=person)(UserPrincipalName={0}))
#ldapUsernameProperty=UserPrincipalName

#
# Example for Active Directory when using DIGEST-MD5 authentication.
#
# When DIGEST-MD5 is enabled for Active Directory, the ldapServerUrl and
# ldapManagerDn need to be specified differently.
#
# The server URL can not be specified as an IP address. It needs to be
# specified as the server's hostname, as AD uses it to resolve the DN
# in some Microsoft AD specific way.
#
# The manager DN can not be specified as a real DN, instead it should
# be specified as the manager's username (sAMAccountName in AD).
# I.e. don't specify the manager's CN, but rather it's login name.
# 
# This changes the first example above as follows.
#

#ldapAuthenticationEnabled=true
#ldapServerUrl=ldap://server.domain.com:3268
#ldapDirectoryType=ActiveDirectory
#ldapAuthenticationMechanism=DIGEST-MD5
#ldapManagerDn=administrator
#ldapManagerPassword=secret
#ldapGroupSearchBase=
#ldapGroupSearchFilter=(&(objectClass=group)(member={0}))
#ldapGroupRoleAttribute=sAMAccountName
#ldapUserSearchBase=cn=Users
#ldapUserSearchFilter=(&(objectClass=person)(sAMAccountName={0}))
#ldapUserObjectClassRestrictions=person



#
# Example for OpenDirectory
#

#ldapAuthenticationEnabled=true
#ldapServerUrl=ldap://yourip:389/dc=yourserver,dc=yourdomain,dc=loc
#ldapDirectoryType=OpenDirectory
#ldapGroupSearchBase=cn=groups
#ldapGroupSearchFilter=(memberUid={1})
#ldapGroupRoleAttribute=cn
#ldapUserSearchBase=cn=users
#ldapUserSearchFilter=(uid={0})
#ldapUserObjectClassRestrictions=person

#
# Example for filtering users or groups returned in user and group search
# dialogs in the desktop client.
#
# This setting can be used in a large ldap where only a few
# users or groups are using Elvis DAM.
#
# These filters do not affect permissions directly.
#

# ldapAdditionalGroupFilter=(cn=Elvis*)
# ldapAdditionalUserFilter=(memberOf=CN=Editors,cn=Users,dc=yourdomain,dc=loc)


############## TASS

ldapAuthenticationEnabled=true
ldapServerUrl=ldaps://ldaps01.tass.ru:636
ldapDirectoryType=ActiveDirectory
ldapAuthenticationMechanism=SIMPLE
ldapManagerDn=CN=LdapReader_Elvis,OU=Service Accounts,DC=corp,DC=tass,DC=ru
ldapManagerPassword=Zb3nhF&M
ldapGroupSearchBase=OU=Elvis,OU=Groups,DC=corp,DC=tass,DC=ru
ldapGroupSearchFilter=(&(objectClass=group)(member={0}))

ldapGroupRoleAttribute=sAMAccountName

ldapUserSearchBase=OU=TASS_Users,DC=corp,DC=tass,DC=ru
ldapUserSearchFilter=(&(objectClass=person)(sAMAccountName={0}))

ldapAdditionalUserFilter=(&(memberOf=CN=DUMMY,OU=Elvis,OU=Groups,DC=corp,DC=tass,DC=ru)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))
#ldapAdditionalUserFilter=(!(userAccountControl:1.2.840.113556.1.4.803:=2))

ldapUserObjectClassRestrictions=person