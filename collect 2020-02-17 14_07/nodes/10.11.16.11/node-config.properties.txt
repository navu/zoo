#Tue, 18 Jun 2019 12:54:19 +0200
#charset=UTF-8
#
# Name of the cluster. Must be set to a unique name for each cluster.
#
# Only nodes using the exact same (case sensitive) clusterName will be able to join to form a cluster.
#
clusterName=TassDAM

#
# Unique name for this node to identify it in the cluster.
#
# If you use nodeName=${randomHeroName}, Elvis will pick a random hero name for this node.
# Another option is to use nodeName=${computerName}, this will use the computer name as configured in the operating system.
# Or you can enter a node name manually, if you do so, make sure each node has a different name.
#
#nodeName=${computerName}-${randomHeroName}
nodeName=${computerName}

#
# IP address to use for internal Elasticsearch and Hazelcast cluster communication.
#
# Always specify this explicitly if you run a multi-node cluster and your machines have multiple network interfaces.
# This IP address will be used to both bind to and publish to other nodes so they can connect back to this node.
#
# The default value ${firstHostIP} is preferably the en0 IP address, localhost IP address or alphabetically first
# non-loopback IP address. ${firstHostIP} will not be accepted on multi-node machines with multiple network interfaces.
# If you are really sure that you want to use the en0 IP address, you can configure ${en0IP}.
#
#cluster.network.host=${firstHostIP}
cluster.network.host=10.11.16.11

#
# Enables node discovery by direct TCP connect to one of the specified ip addresses.
#
# If you are running on Amazon EC2, check the 'cluster.join.aws' settings below.
#
# Make sure the following ports are not blocked by your firewall or security group: 9300, 5701.
# Also make sure all nodes have the exact same 'clusterName'.
#
# When you have lots of nodes you only have to specify a set of 5 or so IP addresses.
# Only one of the nodes needs to respond. Once joined, this node will receive the
# complete set of known cluster nodes already known in the joined cluster.
#
# When 'cluster.join.tcpip.enabled' is false, this setting is ignored.
#
# You can specify a comma separated list of other nodes in the cluster:
#
# cluster.join.tcpip.members=10.11.12.1, 10.11.12.2
#
#cluster.join.tcpip.members=10.11.16.11, 10.11.16.12, 10.11.16.13, 10.22.16.11, 10.22.16.12, 10.22.16.13, 10.33.16.11
cluster.join.tcpip.members=10.11.16.11, 10.11.16.15, 10.11.16.16, 10.22.16.11, 10.22.16.15, 10.22.16.16, 10.33.16.11


#
# When you run on Amazon EC2 you can enable this discovery mechanism to find other
# nodes in the cluster. This connects to the AWS EC2 API and will fetch a list of all your instances.
#
# Your instances must run with an IAM role that grants the following permissions:
#
#   "ec2:DescribeInstances",
#   "ec2:DescribeSecurityGroups"
#
# The list of instance IP addresses provided by AWS is used to find potential
# Elvis nodes with the same clusterName.
#
# You can limit potential instances by specifying the name of a security group that you have assigned to all Elvis
# nodes for this cluster.
#
cluster.join.aws.enabled=false

#
# The EC2 region where the instances will be running.
#
# For example us-east-1 or eu-west-1.
#
cluster.join.aws.region=

#
# A comma separated list of AWS security groups.
# Only instances with the provided security groups will be used in the cluster discovery.
#
# NOTE: You can provide either group NAME or group ID.
#
# cluster.join.aws.securityGroupName=elvis-cluster-sg
#
cluster.join.aws.securityGroupName=

#
# Turns this node into a 'search node', enabling it to hold search index data.
#
# You will need more search nodes when the number of assets in the system grows.
# Using more search nodes will also allow the system to handle more concurrent searches.
#
# Make sure to match the cluster.searchNodeCount in cluster-config.properties.txt to
# the number of nodes in your cluster that have searchDataEnabled=true.
#
# If you have very high search load you may also need to increase the number of shard replicas.
#
searchDataEnabled=true

#
# Turns this node into a 'processing node', enabling it to do file processing.
#
# Processing metadata extraction, embedding and generating previews can be heavy for a machine,
# so preferably do not combine this with searchDataEnabled nodes.
#
# The number of processing nodes you need depends on how many files are being imported and checked in.
#
processingEnabled=false

#
# Turns this node into a 'job runner node', enabling it to execute scheduled (plugin) jobs.
#
# Job runner can generally be included on 'any node' as it is usually less critical than direct API calls.
# If processing is proving to be a bottleneck, include it on a search node, otherwise a processing node.
# In most cases it does not matter that a background job takes a bit longer or is triggered a few seconds later than scheduled.
# However, if there are jobs that are required to run predictably, one or more dedicated 'job runner nodes' should be
# setup, separate from the 'processing nodes'.
#
jobRunnerEnabled=false

#
# Hot data location. This is where the search index and internal database are
# stored. For optimal performance you must put the hot data on the fastest
# storage available.
#
# In most cases this will be the internal RAID set of the server, especially when
# using SAS or even better: SSD. Although in some cases external storage (SAN)
# can be faster since more disks can be read and written at the same time.
#
# This folder does not have to be included in your backup. Since the files in it
# are constantly changing and being written to, they are hard to backup anyway.
# Elvis creates static backups of this data in the main dataLocation every night.
# See 'Backup' plugin.
#
# Note: Use '/' or enter '\\' instead of '\'.
#
#hotDataLocation=/srv/elvis-server/elvis-hot-data
hotDataLocation=/mnt/elvis/elvis-hot-data

#
# Main data location when storage.engine.type is "shared-volume".
# This is where all the originals, previews,
# thumbnails and temporary files are stored.
#
# In most cases this will be a large flexible volume. For example
# using external storage (SAN) connected using a block level protocol
# like iSCSI or Fibre.
#
# When running a cluster, this should be a shared volume mounted
# through for example NFS or SMB.
#
# This folder should be included in your backup procedure.
#
# Note: Use '/' or enter '\\' instead of '\'.
# Note: When the sharedDataLocation is different on each node, make sure that the elasticsearch.backup.location
#   (which references this location by default) is also configured correctly for the cluster. This can be done in the
#   cluster-config.properties.txt file.
#
#sharedDataLocation=/srv/elvis-server/elvis-shared-data
sharedDataLocation=/mnt/nfs/elvis-shared-data


#
# Set to 'local' when using the local filestore as data location.
# By default it is set to shared to indicate it is not a local file store.
#
# This setting is used to determine whether or not a check needs to be performed
# on the sharedDataLocation being mounted.
#
fileStoreType=shared

#
# Port used for HTTP access to the server.
#
# Note: this setting is only used on nodes that have the web API enabled.
#
serverPort=8080
# SAML
samlKeyStore=${configDir}/sso-keystore.jks
samlDefaultKey=elvis-signing
samlKeyStorePassword=OBF:1kqr1s4i1bv61ob71g131ktv1kqz1g2f1odn1bxo1s2k1ku3
serverUrl=https://dam.tass.ru
