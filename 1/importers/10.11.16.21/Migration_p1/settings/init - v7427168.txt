﻿{
  "ora_url": "dmz-db05-n02.dmz.tass.ru",
  "ora_port": "1521",
  "ora_sid": "ORPHEAV4",
  "ora_user": "DAM_LOADER",
  "ora_pass": "ln4K83o&4$c6",
  "ora_schema": "ORPHEA",
  
  "elv_url": "http://10.11.16.13:8080/",
  "elv_user": "api", 
  "elv_pass": "ww",
  "elv_folderPath": "/",
  
  "pack_size": "50",
  "comment_pack_size": "items count per pack",
  "pack_count": "100000",
  "comment_pack_count": "count of the packs",
  "pack_timeout": "5",
  "comment_pack_timeout": "timeout between packs [sec.]",
  "exc_statuses": "",
  "exc_statuses_rep": "",
  "exc_statuses_chut": "",
  "exc_statuses_stock": "",
  "comment_exc_statuses": "excluded statuses, -999: all statuses, empty string: none statuses",
  "logable_status": "",
  "comment_logable_status": "statuses to be written to the table, -999: none of the statuses, empty string: all statuses",
  
 "Names": ["OBJETS.ID_OBJET","OBJETS.ID_TYPE_DOC"],
  "Opers": [6,0],
  "comment_Opers": "0: eq, 1: lt, 2: le, 3: gt, 4: ge, 5: NOT eq, 6: BETWEEN, 7: NOT BETWEEN, 11: IN, 12: LIKE, 13: NOT IN",
  "Values": ["7427168 and 31000000","5"],
  
  "Names_rep": ["LIASSES.ID_LIASSE"],
  "Opers_rep": [6],
  "comment_Opers_rep": "0: eq, 1: lt, 2: le, 3: gt, 4: ge, 5: NOT eq, 6: BETWEEN, 7: NOT BETWEEN, 11: IN, 12: LIKE, 13: NOT IN",
  "Values_rep": [""],

  "Names_chut": ["CHUTIER.ID_CHUTIER"],
  "Opers_chut": [6],
  "comment_Opers_chut": "0: eq, 1: lt, 2: le, 3: gt, 4: ge, 5: NOT eq, 6: BETWEEN, 7: NOT BETWEEN, 11: IN, 12: LIKE, 13: NOT IN",
  "Values_chut": [""],
  
  "Names_stock": ["STOCKS.ID_STOCK"],
  "Opers_stock": [6],
  "comment_Opers_stock": "0: eq, 1: lt, 2: le, 3: gt, 4: ge, 5: NOT eq, 6: BETWEEN, 7: NOT BETWEEN, 11: IN, 12: LIKE, 13: NOT IN",
  "Values_stock": [""],

  "is_checking_names": 0,
  "CheckingNames": ["description", "cf_creditRu"],
  
  "LOG_FILE": "../logs/log-v1-0-31000000.txt",
  "LOG_REVISION_PERIOD_MIN": 30,
  "LOG_FILE_LIMIT_SIZE_KB": 1024,
  "logModules": ["Error", "Elvis", "HTTP", "SQL", "Info"],
  "comment_logModules": ["Error", "Elvis", "HTTP", "SQL", "Info", "Debug"],
  
  "break": 0, 
  "comment_break": "0 - false, 1 - true",
  "check_import_table": 1,
  
  "run_immediately": 1,
  "start_time": "2020.02.04 8:05",
  "program_timeout_min": 1440,
  "start_program_timeout": "2020.12.10 7:00",
  
  "glob_ID_OBJET_List_source": 0,
  "comment_glob_ID_OBJET_List_source": "0 - base, 1 - file",
  "glob_ID_OBJET_List_file": "../settings/id----.txt",
  
  "stockId_deleted": [0, 37, 940]
}