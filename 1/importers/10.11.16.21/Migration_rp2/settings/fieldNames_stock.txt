﻿{
  "cf_isMigrated": { "type": "function", "value": "'true'" },
  "cf_containerType": { "type": "function", "value": "'библиотека'" },
  
  "cf_objectId": "STOCKS.ID_STOCK",
  "cf_headlineRu": "STOCKS.LIB_STOCK",
  "cf_descriptionRu": "STOCKS.DESC_STOCK",
  "assetCreated": "TO_CHAR(STOCKS.DATE_CREAT,'YYYY-MM-DD HH24:MI:SS')",

  "ax_ID_STOCK": "STOCKS.ID_STOCK"
}