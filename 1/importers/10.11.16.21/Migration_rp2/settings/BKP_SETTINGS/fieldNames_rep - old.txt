﻿{
  "cf_containerType": { "type": "function", "value": "'репортаж'" },
  "cf_isMigrated": { "type": "function", "value": "'true'" },
  "status": { "type": "function", "value": "'выпущено на сайт'" },
  
  "cf_headlineEn": "LIASSES.DESC_LIASSE",
  "cf_keyAssetId": "LIASSES.ID_DEFAULTOBJET",
  "cf_objectId": "LIASSES.ID_LIASSE",
  "cf_stockId": "LIASSES.ID_STOCK_LIASSE",
  "cf_headlineRu": "LIASSES.LIB_LIASSE",
  "cf_itemCount": "LIASSES.NB_DOCUMENTS",
  "cf_urgencyRef": "LIASSES.PRIORITY_LIASSE",
  "cf_reportageReferenceId": "LIASSES.REF_REPORTAGE",
  "copyright": "LIASSES.REP_COPYRIGHT",
  "cf_wireSections": "LIASSES.SECTION",
  "assetCreated": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "cf_plannedPubDate": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "cf_releaseDateTime": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "cf_releaseDateTimeReal": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "cf_releaseDateTimeFirst": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "iptcCreated": "TO_CHAR(LIASSES.DATE_LIASSE,'YYYY-MM-DD HH24:MI:SS')",
  "assetCreator": "LIASSES.ID_USER",
  "hidden": "case when LIASSES.PRIORITY_LIASSE=0 then 'true' else NULL end",
  
  "ax_ID_LIASSE": "LIASSES.ID_LIASSE",
  "ax_itemCount": "LIASSES.NB_DOCUMENTS"
}