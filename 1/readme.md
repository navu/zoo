# Протокол импорта

|
 --- | ---
Имя кластера|TassDAM
Версия Elvis|6.42.1.633
Количество узлов|12
|
**Имя узла**|**m2-elvis-s01.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.22.16.11
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m1-elvis-s03.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.16
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m2-elvis-jr01.corp.tass.ru**
Роли|Job/Runner
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.22.16.13
Количество ядер|16
Объем памяти|32G
|
**Имя узла**|**m3-elvis-s01.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.33.16.11
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m1-elvis-pr03.corp.tass.ru**
Роли|Processing
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.18
Количество ядер|16
Объем памяти|32G
|
**Имя узла**|**m1-elvis-jr01.corp.tass.ru**
Роли|Job/Runner
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.13
Количество ядер|16
Объем памяти|32G
|
**Имя узла**|**m2-elvis-s03.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.22.16.16
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m1-elvis-s02.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.15
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m1-elvis-pr01.corp.tass.ru**
Роли|Processing
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.12
Количество ядер|16
Объем памяти|32G
|
**Имя узла**|**m2-elvis-s02.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.22.16.15
Количество ядер|16
Объем памяти|64G
|
**Имя узла**|**m1-elvis-pr02.corp.tass.ru**
Роли|Processing
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.17
Количество ядер|16
Объем памяти|32G
|
**Имя узла**|**m1-elvis-s01.corp.tass.ru**
Роли|Search/Web/API
Операционная система|CentOS Linux release 7.7.1908 (Core)
IP адрес|10.11.16.11
Количество ядер|16
Объем памяти|64G
|
**Документы Google:**
DAM: разделы (библиотеки) ТФК
DAM: Реестр метаданных
|
**Импортеры:**
Адрес узла|10.11.16.21
Адрес узла|10.11.16.22
